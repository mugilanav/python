

# In[9]:
# In[1]:
# 2)	Write a python script to transform the data available in the Data Files using Python/Pandas to be able to fit into a typical Database table. Aggregate the sales of both the branches and load the cleansed and transformed data into a CSV file (final.csv) that can be easily loaded into a table.

# Come up with the table DDL with all the necessary fields/columns needed. (Refer to Data Files: Branch1_Sales.csv, Branch2_Sales.csv; Files contain sales information of two different Convenient store branches; As of now, data is only made available until Feb 2021)


import pandas as pd


# In[2]:


branch1 = pd.read_csv('Documents\\Branch1_Sales.csv')


# In[45]:


branch1['PRODUCT_TYPE'] = branch1['PRODUCT_TYPE'].str.title()


# In[46]:


branch1


# In[47]:


branch2 = pd.read_csv('Documents\\Branch2_Sales.csv')


# In[48]:


branch2['PRODUCT_TYPE'] = branch2['PRODUCT_TYPE'].str.title()


# In[49]:


branch2


# In[50]:


branch_aggregate = pd.concat([branch1, branch2], axis=0)


# In[72]:


branch_aggregate


# In[75]:


merge = branch_aggregate.groupby(['PRODUCT_TYPE']).agg('sum') 


# In[76]:


merge


# In[85]:


v = v.reset_index()


# In[86]:


v


# In[95]:


final = v.melt(id_vars=['PRODUCT_TYPE'], var_name='SOLD_DATE', value_name= 'QTY_SOLD')


# In[96]:


final


# In[98]:


final=final[final.columns[[1,0,2]]]


# In[115]:


final


# In[101]:


final.to_csv('Documents/final.csv')




