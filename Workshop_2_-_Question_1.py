#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# 1)	Write the Python program for the below representation of Class (using inheritance) and create objects.

class Person:
  def __init__(self, name, designation):
    self.name = name
    self.designation = designation

  def learn(self):
    print('I am', self.name,'.', 'I am a', self.designation,'.','I know how to learn things myself')
    
  def walk(self):
    print('I am', self.name,'.', 'I am a', self.designation,'.','I know how to walk')
    
  def eat(self):
    print('I am', self.name,'.', 'I am a', self.designation,'.','I know how to eat')

class Programmer(Person):
  def __init__(self, name, designation, companyName):
    super().__init__(name, designation)
    self.companyName = companyName
    Person.learn(self)
    Person.walk(self)
    Person.eat(self)

  def coding(self):
    print('I am', self.name,'.', 'I am a', self.designation,'.',"I work at", self.companyName)
    
class Dancer(Person):
  def __init__(self, name, designation, groupName):
    super().__init__(name, designation)
    self.groupName = groupName
    Person.learn(self)
    Person.walk(self)
    Person.eat(self)

  def dancing(self):
    print('I am', self.name,'.', 'I am a', self.designation,'.',"I belong to the band called", self.groupName)
    
class Singer(Person):
  def __init__(self, name, designation, bandName):
    super().__init__(name, designation)
    self.bandName = bandName
    Person.learn(self)
    Person.walk(self)
    Person.eat(self)

  def singing(self):
    print('I am', self.name,'.', 'I am a', self.designation,'.',"My band is", self.bandName, " and I sing there")
    
  def playGuitar(self):
    print('I am', self.name,'.', 'I am a', self.designation,'.', "My band is", self.bandName, " and I play Guitar there")
    
a = Programmer("Mugilan", "Data Engineer", "Systech")
a.coding()





