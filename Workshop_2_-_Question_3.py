#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd 
import requests
import json


# In[6]:


data = requests.get("https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=2017-01-01&endtime=2017-12-31&alertlevel=yellow")
data = json.loads(data.text)
a = data["features"]         
df = pd.DataFrame(a)  
b = df.properties 
c = pd.json_normalize(b)
c


# In[2]:


import pyodbc 
server = 'OTUSDPSQL' 
database = 'B12022_Target_MArumuga' 
username = 'B12022_MArumuga' 
password = '2025Bm@0099' 
cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
cursor = cnxn.cursor()


# In[3]:


cursor.execute('create table api(Place varchar(50), Type varchar(25), Magnitude numeric(3,1), Title varchar(50))')


# In[7]:


for index,row in c.iterrows():
    cursor.execute("INSERT INTO dbo.api values(?,?,?,?)", row['place'],row['type'],row['cdi'],row['title'])


# In[ ]:


cnxn.commit()


# In[8]:


cursor.close()


# In[9]:



c.to_csv('Documents/api.csv')


# In[10]:


import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('Documents/api.csv')

df.plot()

plt.show()


# In[ ]:




